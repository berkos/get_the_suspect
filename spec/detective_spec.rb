require 'spec_helper'

describe Detective do
  let(:detective) { Detective.new }

  describe "#calculate_location" do
    it 'return an array with the location x,y' do

      stub_request(:get, "http://which-technical-exercise.herokuapp.com/api/aberkakis@gmail.com/directions").
         with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.1'}).
         to_return(:status => 200, :body => "{\"directions\":[\"forward\",\"right\",\"forward\",\"forward\",\"forward\",\"left\",\"forward\",\"forward\",\"left\",\"right\",\"forward\",\"right\",\"forward\",\"forward\",\"right\",\"forward\",\"forward\",\"left\"]}", :headers => {})
      
      expect(detective.calculate_location).to  match_array([5, 2])
    end
    it 'raises an error as the api responded with 500 error code' do
      stub_request(:get, "http://which-technical-exercise.herokuapp.com/api/aberkakis@gmail.com/directions").
         with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.1'}).
         to_return(:status => 500, :body => "", :headers => {})

      expect{ detective.calculate_location }.to raise_error(RuntimeError, "No Connection With The Authorities")
    end

    it 'raises an error as the api responded with 400 error code' do
      stub_request(:get, "http://which-technical-exercise.herokuapp.com/api/aberkakis@gmail.com/directions").
         with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.1'}).
         to_return(:status => 400, :body => "", :headers => {})

      expect{ detective.calculate_location }.to raise_error(RuntimeError, "No Authorized for this type of resource")
    end
  end

  describe "#call_back_up" do
    it 'finds the lost kittens' do
      stub_request(:get, "http://which-technical-exercise.herokuapp.com/api/aberkakis@gmail.com/location/5/2").
         with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.1'}).
         to_return(:status => 200, :body => "{\"message\":\"Congratulations! The search party successfully recovered the missing kittens. Please zip up your code and send it to chris.patuzzo@which.co.uk\"}", :headers => {})

      expect(detective.call_back_up([5,2])).to  eq "Good Job! We got the suspect"
    end
    it 'does not find the kittens' do
      stub_request(:get, "http://which-technical-exercise.herokuapp.com/api/aberkakis@gmail.com/location/5/2").
         with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.1'}).
         to_return(:status => 200, :body => "{\"message\":\"Unfortunately, the search party failed to recover the missing kittens. You have 4 attempts remaining.\"}", :headers => {})

      expect(detective.call_back_up([5,2])).to  eq "not good guess!"
    end
    it 'shows a friendly message that you have already recovered the kittens' do
      stub_request(:get, "http://which-technical-exercise.herokuapp.com/api/aberkakis@gmail.com/location/5/2").
         with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.9.1'}).
         to_return(:status => 200, :body => "{\"message\":\"The search party has already recovered the kittens and they're happily back at home. Please zip up your code and send it to chris.patuzzo@which.co.uk\"}", :headers => {})
      
      expect(detective.call_back_up([5,2])).to   eq "Good Job! You have already recovered the kittens"
    end
  end
end