require 'faraday'
require 'json'

class Detective
  def initialize
    @x = 0
    @y = 0
    @facing = 0 # set the facing at north
  end

  def call_back_up(location)
    response = api_connection "/api/aberkakis@gmail.com/location/#{location[0]}/#{location[1]}"
    if response['message'].include? 'Congratulations!'
       "Good Job! We got the suspect"
    elsif response['message'].include? 'has already recovered'
       "Good Job! You have already recovered the kittens"
    else
       "not good guess!"
    end
  end

  def calculate_location
    directions = api_connection '/api/aberkakis@gmail.com/directions'

    directions['directions'].each do |direction|
      case direction
      when 'forward'
        case @facing
        when 0 #north
          @y = @y + 1
        when 1 #east
          @x = @x + 1
        when 2 #south
          @y = @y - 1
        when 3 #west
          @x = @x -1
        end
      when 'right'
        @facing = @facing + 1
      when 'left'
        @facing = @facing - 1
      end
    end

    [@x, @y]
  end

  private

  def api_connection(uri)
    conn = Faraday.new(:url => 'http://which-technical-exercise.herokuapp.com') do |faraday|
      faraday.request  :url_encoded             # form-encode POST params
      faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
    end

    response = conn.get uri

    raise 'No Connection With The Authorities'        if response.status.to_i.between?(500, 599)
    raise 'No Authorized for this type of resource'   if response.status.to_i.between?(400, 499)
    
    data = JSON.parse(response.body)
  end
end